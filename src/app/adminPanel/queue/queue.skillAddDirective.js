/**
 * Created by mumar on 1/11/2016.
 */
(function ()
{
  'use strict';

  angular
    .module('app.queue')
    .controller('msSbAddSkillController', msSbAddSkillController)
    .directive('msSbAddSkill', msSbAddSkill);

  /** @ngInject */
  function msSbAddSkillController($scope, $timeout, msUtils,api,$q)
  {
    var vm = this;
    var formData = {id:'',skill_level:''};
    vm.newCardName = '';
    vm.msQueueId = $scope.msQueue.id;
    vm.msQueue = $scope.msQueue;
    vm.skillIds=[];
    vm.ids=[];
    vm.skillList=[];
    vm.data = {};
    getSkillData().then(function(res){
      getAgentData().then(function(ress){
        vm.skillList=[];
        vm.groupList=[];
        vm.agentList=[];
        vm.data = res;
        vm.dataA = ress;
        vm.skillList[vm.msQueueId] = vm.data.skillList;
        vm.groupList[vm.msQueueId] = vm.data.groupList;
        vm.agentList[vm.msQueueId] = ress.agentList;
        vm.skillIds[vm.msQueueId]=[];
        vm.ids[vm.msQueueId]=[];
        vm.getIds();
      });



    },function(err){});


    // Methods
    vm.addNewSkill = addNewSkill;
    vm.skillName = skillName;
    vm.getIds = getIds;


    function getIds(){
      if(vm.msQueue.queueing_model===1)
        vm.ids[vm.msQueueId] = _.difference(_.map(vm.skillList[vm.msQueueId],'id'),_.map(vm.msQueue.skillId,'id'));
      if(vm.msQueue.queueing_model===2)
        vm.ids[vm.msQueueId] = _.difference(_.map(vm.agentList[vm.msQueueId],'userId'),vm.msQueue.agentId);
    };


    function getSkillData()
    {
      // Create a new deferred object
      var deferred = $q.defer();

      api.queue.list.get({},

        // SUCCESS
        function (response)
        {
          // Attach the data
          vm.skillList = response.data;
          deferred.resolve(response);
        },

        // ERROR
        function (response)
        {
          console.log(response);
          // Reject the promise
          deferred.reject(response);
        }
      );

      return deferred.promise;
    }

    function getAgentData()
    {
      // Create a new deferred object
      var deferred = $q.defer();

      api.agent.list.get({},

        // SUCCESS
        function (response)
        {
          // Attach the data
          vm.skillList = response.data;
          deferred.resolve(response);
        },

        // ERROR
        function (response)
        {
          console.log(response);
          // Reject the promise
          deferred.reject(response);
        }
      );

      return deferred.promise;
    }
    function skillName(id){
      if(vm.msQueue.queueing_model===1)
      return _.result(_.find(vm.data.skillList, {id:id}), 'name');
      if(vm.msQueue.queueing_model===2)
        return _.result(_.find(vm.dataA.agentList, {userId:id}),'first_name');
    };
    /////


    /**
     * Add New Card
     */
    function addNewSkill()
    {
      if ( vm.formData.id === '' || vm.formData.skill_level === ''  )
      {
        return;
      }

        //var index = _.findIndex(vm.queueList,{id:vm.msSkillId});
      //vm.queueList[index].skillId.push(vm.skill.id);
      if(vm.msQueue.queueing_model===1){
       if(vm.msQueue.skillId===undefined)
         vm.msQueue.skillId=[];
        vm.msQueue.skillId.push(vm.formData);
      }

      if(vm.msQueue.queueing_model===2){
        if(vm.msQueue.agentId===undefined){
          vm.msQueue.agentId = [];
        }
        vm.msQueue.agentId.push(vm.formData.id);
      }

      vm.getIds();

      $timeout(function ()
      {
        $scope.scrollListContentBottom();
      });

      vm.formData = '';
      //vm.formData.skill_level = '';
    }
  }

  /** @ngInject */
  function msSbAddSkill($document, $compile, $timeout)
  {
    return {
      restrict    : 'A',
      controller  : 'msSbAddSkillController',
      controllerAs: 'vm',
      scope       : {
        msQueueId: '=',
        msQueue: '='
      },
      link        : function (scope, iElement)
      {
        scope.closeForm = closeForm;
        scope.scrollListContentBottom = scrollListContentBottom;

        var form = '<md-divider></md-divider><div class="addSkillAndGroup"><form ng-submit="vm.addNewSkill()" name="addingInQueue"  layout="column">\n\n   ' +
            ' <md-input-container  flex md-no-float>\n        <md-select required ng-model="vm.formData.id" name="skill" placeholder="Select one" >' +
          '<md-option ng-value="sk"  ng-repeat="sk in vm.ids[vm.msQueueId]">{{ vm.skillName(sk) }}</md-option></md-select>\n    </md-input-container>\n\n ' +
          ' <md-input-container flex md-no-float>\n       ' +  ' <div ng-messages="addingInQueue.skill.$error" ng-show="addingInQueue.skill.$touched" role="alert">'+
        '<div ng-message="required">'+
        '<span>Select One</span>'+
        '</div>'+
        '</div>';
        if(scope.msQueue.queueing_model===1)
        form = form +  '<md-slider md-discrete="" flex="60" min="1" max="10" ng-model="vm.formData.skill_level" aria-label="skill_level" class="">  </md-slider>';
        if(scope.msQueue.queueing_model===2)
          form = form +  ' <input ng-if="scope.msQueue.queueing_model===2" placeholder="Service Level" type="hidden" autocomplete="off"\n       ' +
          '        ng-model="vm.formData.skill_level" >\n   ' ;

        form = form + ' </md-input-container>\n\n   '+
            '    <div layout="row" layout-align="space-between center">\n       ' +
            ' <md-button type="submit"\n   ng-disabled="addingInQueue.$invalid || addingInQueue.$pristine"                class="add-button md-accent md-raised"\n                   aria-label="add" >\n            <span>Add</span>\n        </md-button>\n        <md-button ng-click="closeForm()" class="cancel-button md-icon-button"\n                   aria-label="cancel" >\n            <md-icon md-font-icon="icon-close"></md-icon>\n        </md-button>\n    </div>\n\n</form></div>';
          var formEl = '',
          listContent = iElement.prev();

        /**
         * Click Event
         */
        iElement.on('click', function (event)
        {
          event.preventDefault();
          openForm();
        });

        /**
         * Open Form
         */
        function openForm()
        {

          iElement.hide();

          formEl = $compile(form)(scope);

          listContent.append(formEl);

          scrollListContentBottom();

          formEl.find('input').focus();

          $timeout(function ()
          {
            $document.on('click', outSideClick);
          });
        }

        /**
         * Close Form
         */
        function closeForm()
        {
          formEl.remove();

          iElement.next().remove();

          iElement.show();

          PerfectScrollbar.update(listContent[0]);

          // Clean
          $document.off('click', outSideClick);
          scope.$on('$destroy', function ()
          {
            $document.off('click', outSideClick);
          });
        }

        /**
         * Scroll List to the Bottom
         */
        function scrollListContentBottom()
        {
          listContent[0].scrollTop = listContent[0].scrollHeight;
        }

        /**
         * Click Outside Event Handler
         * @param event
         */
        var outSideClick = function (event)
        {
          var isChild = formEl.has(event.target).length > 0;
          var isSelf = formEl[0] == event.target;
          var isInside = isChild || isSelf;

          if ( !isInside )
          {
            closeForm();
          }
        }

      }
    };
  }
})();
