/**
 * Created by mumar on 1/11/2016.
 */

(function(){
  'use strict';
  angular
    .module('app.queue')
    .controller('QueueController',queueListCtrl)
    .controller('RightCtrl',RightCtrl);


  function queueListCtrl($scope,QueueData,AgentData,DialogServiceQueue,msUtils,$mdSidenav,$log,$mdDialog,$document){
    var vm = this;

    //Data
    vm.queueList = QueueData.queueList;
    vm.skillList = QueueData.skillList;
    vm.groupList = QueueData.groupList;
    vm.agentList = AgentData.agentList;
    vm.sortableListOptions = {
      axis       : 'x',
      delay      : 75,
      distance   : 7,
      items      : '> .list-wrapper',
      handle     : '.list-header',
      placeholder: 'list-wrapper list-sortable-placeholder',
      tolerance  : 'pointer',
      start      : function (event, ui)
      {
        var width = ui.item[0].children[0].clientWidth;
        var height = ui.item[0].children[0].clientHeight;
        ui.placeholder.css({
          'min-width': width + 'px',
          'width'    : width + 'px',
          'height'   : height + 'px'
        });
      }
    };
    vm.sortableCardOptions = {
      appendTo            : 'body',
      connectWith         : '.list-cards',
      delay               : 75,
      distance            : 7,
      forceHelperSize     : true,
      forcePlaceholderSize: true,
      handle              : msUtils.isMobile() ? '.list-card-sort-handle' : false,
      helper              : function (event, el)
      {
        return el.clone().addClass('list-card-sort-helper');
      },
      update:function(e, ui){
        //console.log(ui.item.sortable);
        //console.log(ui.item.scope);
        console.log("updated");
        console.log(ui.item.sortable.model);
        console.log(ui.item.sortable.sourceModel);
      },
      stop:function(e,ui){
        console.log("stopped");
        console.log(ui.item.sortable.model);
        console.log(ui.item.sortable.sourceModel);
      }
      ,
      placeholder         : 'list-card card-sortable-placeholder',
      tolerance           : 'pointer',
      scroll              : true,
      sort                : function (event, ui)
      {
        var listContentEl = ui.placeholder.closest('.list-content');
        var boardContentEl = ui.placeholder.closest('#board');

        if ( listContentEl )
        {
          var listContentElHeight = listContentEl[0].clientHeight,
            listContentElScrollHeight = listContentEl[0].scrollHeight;

          if ( listContentElHeight !== listContentElScrollHeight )
          {
            var itemTop = ui.position.top,
              itemBottom = itemTop + ui.item.height(),
              listTop = listContentEl.offset().top,
              listBottom = listTop + listContentElHeight;

            if ( itemTop < listTop + 25 )
            {
              listContentEl.scrollTop(listContentEl.scrollTop() - 25);
            }

            if ( itemBottom > listBottom - 25 )
            {
              listContentEl.scrollTop(listContentEl.scrollTop() + 25);
            }
          }
        }

        if ( boardContentEl )
        {
          var boardContentElWidth = boardContentEl[0].clientWidth;
          var boardContentElScrollWidth = boardContentEl[0].scrollWidth;

          if ( boardContentElWidth !== boardContentElScrollWidth )
          {
            var itemLeft = ui.position.left,
              itemRight = itemLeft + ui.item.width(),
              boardLeft = boardContentEl.offset().left,
              boardRight = boardLeft + boardContentElWidth;

            if ( itemLeft < boardLeft + 25 )
            {
              boardContentEl.scrollLeft(boardContentEl.scrollLeft() - 25);
            }

            if ( itemRight > boardRight)
            {
              boardContentEl.scrollLeft(boardContentEl.scrollLeft() + 25);
            }
          }
        }
      }
    };

    vm.openCreateQueueDialog = openCreateQueueDialog;
    vm.openEditQueueDialog = openEditQueueDialog;

    //Method
    vm.getSkillName = skillName;
    vm.getAgentName = agentName;
    vm.deleteCard = deleteCard;
    vm.deleteQueue = deleteQueue;

    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function(){
      return $mdSidenav('right').isOpen('lg');
    };


    function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }
    }
    function skillName(id){

      return _.result(_.find(vm.skillList, {id:id}), 'name');
    };

    function agentName(id){
      return _.find(vm.agentList, {userId:id});
    };
    function deleteQueue(eent , queueId){
      _.remove(vm.queueList, function(queue){
        return queue.id===queueId;
      });
    }
    function openCreateQueueDialog(ev)
    {
      $mdDialog.show({
        templateUrl        : 'app/adminPanel/queue/dialog/createQueue.html',
        controller         : "CreateQueueController",
        controllerAs       : "vm",
        parent             : $document.find('#scrumboard'),
        targetEvent        : ev,
        clickOutsideToClose: true,
        escapeToClose      : true
      }).then(function(response){
        var count = vm.queueList +1;
        response.id=count;
        vm.queueList.push(response);
      })

    }
    function deleteCard(Id,queueId,type){
      var indx = _.findIndex(vm.queueList,{id:queueId});

      if(type==='group'){
        var queu = vm.queueList[indx].agentId;
        _.remove(vm.queueList[indx].agentId, function(id){
          return id===Id;
        });
      }else{
        var queu = vm.queueList[indx].skillId;
        _.remove(vm.queueList[indx].skillId, function(id){
          return id===Id;
        });

      }

      // vm.queueList[indx].agentId[_.findIndex(queu,agentId)]=undefined;
    }
    function openEditQueueDialog(ev,queue)
    {
      $mdDialog.show({
        templateUrl        : 'app/adminPanel/queue/dialog/editQueue.html',
        controller         : "EditQueueController",
        controllerAs       : "vm",
        parent             : $document.find('#scrumboard'),
        targetEvent        : ev,
        clickOutsideToClose: true,
        escapeToClose      : true,
        locals:{
          queueData:queue
        }
      }).then(function(response){

        vm.queueList[_.findIndex(vm.queueList,{id:response.id})]= response

      })

    }
  }

  function RightCtrl($scope,$mdComponentRegistry,$stateParams){
    var vm = this;
    $mdComponentRegistry
      .when('right')
      .then( function(sideNav){

        $scope.isOpen = $stateParams.id;
        vm.toggle = angular.bind( sideNav, sideNav.toggle );

      });
  }
}
)();
