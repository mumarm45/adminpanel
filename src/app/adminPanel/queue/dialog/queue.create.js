/**
 * Created by mumar on 1/13/2016.
 */
(function(){
  'use strict';

  angular
    .module('app.queue')
    .controller('CreateQueueController',createQueueController)
    .directive('requireIfTrue',requireIfTrue);



  function createQueueController($mdDialog,DialogServiceQueue){
    var vm = this;

    vm.closeDialog= closeDialog;
    vm.createQueue=createQueue;
    vm.queuingModel = [{id:1,name:"Resource Skill"},{id:2,name:"Resource Group"}];
    vm.promptlList = [{id:1,name:"welcome.wav"},{id:2,name:"goodbye.wav"},{id:2,name:"goodbye.wav"},{id:2,name:"silent.wav"},{id:4,name:"working.wav"}];



    function createQueue(queue){
      queue.id='';
      $mdDialog.hide(queue);

    }
    function closeDialog()
    {
      $mdDialog.hide();
    }
  }
  function requireIfTrue($compile) {
    return {
      require: '?ngModel',
      link: function (scope, el, attrs, ngModel) {
        if (!ngModel) {
          return;
        }

        if(attrs.requireiftrue==="true"){
          console.log('should require');
          el.attr('required', true);
          el.removeAttr('requireiftrue');
          $compile(el[0])(scope);
        }
        else{
          console.log('should not require');
        }
      }
    };
  };
})();
