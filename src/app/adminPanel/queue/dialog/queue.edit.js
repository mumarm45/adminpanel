/**
 * Created by mumar on 1/14/2016.
 */

(function(){
  'use strict';

  angular
    .module('app.queue')
    .controller('EditQueueController',editQueueController);



  function editQueueController($mdDialog,queueData){
    var vm = this;

    vm.closeDialog= closeDialog;
    vm.queue=queueData;
    vm.editQueue=editQueue;
    vm.queuingModel = [{id:1,name:"Resource Skill"},{id:2,name:"Resource Group"}];
    vm.promptlList = [{id:1,name:"welcome.wav"},{id:2,name:"goodbye.wav"},{id:2,name:"goodbye.wav"},{id:2,name:"silent.wav"},{id:4,name:"working.wav"}];



    function editQueue(queue){
      $mdDialog.hide(queue);

    };
    function closeDialog()
    {
      $mdDialog.hide();
    }
  }
})();
