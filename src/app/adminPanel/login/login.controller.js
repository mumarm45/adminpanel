(function ()
{
    'use strict';

    angular
        .module('app.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($state)
    {
      var vm =this;
        // Data
        vm.signIn = signIn;

        // Methods
function signIn(form){
  $state.go('app.agent')
}
        //////////

    }
})();
