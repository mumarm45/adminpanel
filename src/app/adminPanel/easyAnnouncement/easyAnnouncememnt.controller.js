/**
 * Created by mumar on 1/18/2016.
 */
(function(){

  'use strict';
  angular
    .module('app.easyAnnouncement')
    .controller('EasyAnnouncementController',easyAnnouncementController);


  function easyAnnouncementController($mdDialog, $document,$mdSidenav){
    var vm = this;


    //Date
    // Data
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    //Methods
    // Methods
    vm.addEvent = addEvent;
    vm.next = next;
    vm.prev = prev;
    vm.events = [
      [
        {
          id   : 1,
          title: 'All Day Event',
          start: new Date(y, m, 1),
          end  : null,
          color:'red'
        },
        {
          id   : 2,
          title: 'Long Event',
          start: new Date(y, m, d - 5),
          end  : new Date(y, m, d - 2)
        },
        {
          id   : 3,
          title: 'Some Event',
          start: new Date(y, m, d - 3, 16, 0),
          end  : null
        },
        {
          id   : 4,
          title: 'Repeating Event',
          start: new Date(y, m, d + 4, 16, 0),
          end  : null
        },
        {
          id   : 5,
          title: 'Birthday Party',
          start: new Date(y, m, d + 1, 19, 0),
          end  : new Date(y, m, d + 1, 22, 30)
        },
        {
          id   : 6,
          title: 'All Day Event',
          start: new Date(y, m, d + 8, 16, 0),
          end  : null
        },
        {
          id   : 7,
          title: 'Long Event',
          start: new Date(y, m, d + 12, 16, 0),
          end  : null
        },
        {
          id   : 8,
          title: 'Repeating Event',
          start: new Date(y, m, d + 14, 2, 0),
          end  : null
        },
        {
          id   : 9,
          title: 'Repeating Event',
          start: new Date(y, m, d + 14, 4, 0),
          end  : null
        },
        {
          id   : 10,
          title: 'Repeating Event',
          start: new Date(y, m, d + 14, 2, 0),
          end  : null
        },
        {
          id   : 11,
          title: 'Repeating Event',
          start: new Date(y, m, d + 14, 4, 0),
          end  : null
        },
        {
          id   : 12,
          title: 'Repeating Event',
          start: new Date(y, m, d + 14, 2, 0),
          end  : null
        },
        {
          id   : 13,
          title: 'Repeating Event',
          start: new Date(y, m, d + 14, 4, 0),
          end  : null
        },
        {
          id   : 14,
          title: 'Conference',
          start: new Date(y, m, d + 17, 4, 0),
          end  : null
        },
        {
          id   : 15,
          title: 'Meeting',
          start: new Date(y, m, d + 22, 4, 0),
          end  : new Date(y, m, d + 24, 4, 0)
        }
      ]
    ];
    vm.calendarUiConfig = {
      calendar: {
        editable     : true,
        eventLimit   : true,
        displayEventTime:true,
        dayPopoverFormat :'dddd, MMMM D',
          eventColor: 'rgb(3,155,229)',
        eventTextColor : '#FFFFFF',
        timeFormat: 'H(:mm)',
        header       : '',
        views: {
          month: { // name of view
            titleFormat: 'YYYY, MM, DD'
            // other view-specific options here
          }
        },
        dayNames     : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        viewRender   : function (view)
        {
          vm.calendarView = view;
          vm.calendar = vm.calendarView.calendar;
          vm.currentMonthShort = vm.calendar.getDate().format('MMM');
        },
        columnFormat : {
          month: 'ddd',
          week : 'ddd M',
          day  : 'ddd M'
        },
        eventClick   : eventDetail,
        selectable   : true,
        selectHelper : true,
        dragScroll  : true,
        select       : select
      }
    };
    vm.toggleSidenav = toggleSidenav;
    vm.eventDetail = eventDetail;
    function next()
    {
      vm.calendarView.calendar.next();
    }
    function toggleSidenav(sidenavId)
    {
      $mdSidenav(sidenavId).toggle();
    }
    /**
     * Go to previous on current view (week, month etc.)
     */
    function prev()
    {
      vm.calendarView.calendar.prev();
    }


    function eventDetail(calendarEvent, e)
    {
      showEventDetailDialog(calendarEvent, e);
    }

    /**
     * Add new event in between selected dates
     *
     * @param start
     * @param end
     * @param e
     */
    function select(start, end, e)
    {
      showEventFormDialog('add', false, start, end, e);
    }

    /**
     * Add event
     *
     * @param e
     */
    function addEvent(e)
    {
      var start = new Date(),
        end = new Date();

      showEventFormDialog('add', false, start, end, e);
    }

    /**
     * Show event detail dialog
     * @param calendarEvent
     * @param e
     */
    function showEventDetailDialog(calendarEvent, e)
    {
      $mdDialog.show({
        controller         : 'EventDetailDialogController',
        controllerAs       : 'vm',
        templateUrl        : 'app/adminPanel/easyAnnouncement/dialog/event-details/easyAnnouncement-details-dialog.html',
        parent             : angular.element($document.body),
        targetEvent        : e,
        clickOutsideToClose: true,
        locals             : {
          calendarEvent      : calendarEvent,
          showEventFormDialog: showEventFormDialog,
          event              : e
        }
      });
    }

    function showEventFormDialog(type, calendarEvent, start, end, e)
    {
      var dialogData = {
        type         : type,
        calendarEvent: calendarEvent,
        start        : start,
        end          : end
      };

      $mdDialog.show({
        controller         : 'EventFormDialogController',
        controllerAs       : 'vm',
        templateUrl        : 'app/adminPanel/easyAnnouncement/dialog/event-form/easyAnnouncement-form-dialog.html',
        parent             : angular.element($document.body),
        targetEvent        : e,
        clickOutsideToClose: true,
        locals             : {
          dialogData: dialogData
        },views: {
          month: { // name of view
            titleFormat: 'YYYY, MM, DD'
            // other view-specific options here
          }
        }
      }).then(function (response)
      {
        if ( response.type === 'add' )
        {
          // Add new
          for(var i =0; i<response.calendarEvent.length;i++){
            console.log(response.calendarEvent[i]);
            vm.events[0].push({
              id   : vm.events[0].length + 20,
              title: response.calendarEvent[i].title,
              start: response.calendarEvent[i].start,
              end  : response.calendarEvent[i].end,
              allDay  : response.calendarEvent[i].allDay,
              repeat:response.calendarEvent[i].repeat,
              repeatType:response.calendarEvent[i].repeatType,
              announcements:response.calendarEvent[i].announcements,
              regions:response.calendarEvent[i].regions,
              placeHolder:response.calendarEvent[i].placeHolder,
              color:response.calendarEvent[i].color,
              weekly:response.calendarEvent[i].weekly,
              monthly:response.calendarEvent[i].monthly,
              daily:response.calendarEvent[i].daily,
              description:response.calendarEvent[i].description
            });
          }

        }
        else
        {
          for ( var i = 0; i < vm.events[0].length; i++ )
          {
            // Update
            if ( vm.events[0][i].id === response.calendarEve.id )
            {
              vm.events[0][i] = {
                title: response.calendarEvent.title,
                start: response.calendarEvent.start,
                end  : response.calendarEvent.end
              };

              break;
            }
          }
        }
      });
    }
  }


})();
