/**
 * Created by mumar on 1/18/2016.
 */
(function(){
  'use strict';

  angular
    .module('app.easyAnnouncement',['ui.calendar',
      'ngAnimate',
      'ui.timepicker',
      'color.picker'
    ])
  .config(config);

  function config($stateProvider,msNavigationServiceProvider){

    // State
    $stateProvider.state('app.easyAnnouncement', {
      url      : '/easyAnnouncement',
      views    : {
        'content@app': {
          templateUrl: 'app/adminPanel/easyAnnouncement/easyAnnouncement.html',
          controller : 'EasyAnnouncementController as vm'
        }
      },
      bodyClass: 'calendar'
    });


    // Navigation
    msNavigationServiceProvider.saveItem('easyAnnouncement', {
      title : 'Easy Announcements',
      icon  : 'icon-calendar-today',
      state : 'app.easyAnnouncement',
      weight: 1
    });

  };

})();
