/**
 * Created by mumar on 1/18/2016.
 */
(function ()
{
  'use strict';

  angular.module('app.easyAnnouncement')
    .controller('EventDetailDialogController', EventDetailDialogController);

  /** @ngInject */
  function EventDetailDialogController($mdDialog, calendarEvent, showEventFormDialog, event)
  {
    var vm = this;
    vm.promptList = [
      {id:1,name:"welcome.wav"},
      {id:2,name:"goodbye1.wav"},
      {id:3,name:"goodbye.wav"},
      {id:4,name:"silent.wav"},
      {id:5,name:"working.wav"},
      {id:6,name:"welcome1.wav"},
      {id:7,name:"goodbye2.wav"},
      {id:8,name:"goodby3.wav"},
      {id:9,name:"silen4.wav"},
      {id:10,name:"workin5.wav"},
      {id:11,name:"welcom2.wav"},
      {id:12,name:"goodbye31.wav"},
      {id:13,name:"goodbye33.wav"},
      {id:14,name:"silent33.wav"},
      {id:15,name:"working33.wav"}
    ];
    vm.regionList = [{id:1,name:'Lahore',prefix:'042'},{id:2,name:'Karachi',prefix:'0423'},{id:3,name:'Islamabad',prefix:'0420'},
      {id:4,name:'Sawat',prefix:'052'},{id:5,name:'Faislabad',prefix:'0426'},
      {id:6,name:'Pinidi',prefix:'0422'}];

    // Data
    vm.calendarEvent = calendarEvent;
    if ( !moment.isMoment(vm.calendarEvent.start) )
    {
      if(vm.calendarEvent.start)
      vm.calendarEvent.start = moment(vm.calendarEvent.start);

    }
    if ( !moment.isMoment(vm.calendarEvent.end) )
    {
      if(vm.calendarEvent.end)
      vm.calendarEvent.end = moment(vm.calendarEvent.end);

    }

    // Methods
    vm.editEvent = editEvent;
    vm.closeDialog = closeDialog;
    vm.promptName = promptName;
    vm.regionName = regionName;
    //////////
    function promptName(id){
      return _.result(_.find(vm.promptList, {id:id}), 'name');
    }
    function regionName(id){
      return _.result(_.find(vm.regionList, {id:id}), 'name');
    }
    function closeDialog()
    {
      $mdDialog.hide();
    }

    function editEvent(calendarEvent)
    {
      showEventFormDialog('edit', calendarEvent, false, false, event);
    }
  }
})();
