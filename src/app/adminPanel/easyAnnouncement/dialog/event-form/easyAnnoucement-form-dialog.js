/**
 * Created by mumar on 1/18/2016.
 */
(function ()
{
  'use strict';

  angular.module('app.easyAnnouncement')
    .controller('EventFormDialogController', EventFormDialogController)
    .directive('dateDirective', dateDirective)
    .directive('timePicker', timePicker)
    .directive('datePicker', datePicker)
    .directive('datePair',datePair);

  /** @ngInject */
  function EventFormDialogController($mdDialog, dialogData,$filter,msUtils,$q)
  {
    var vm = this;

    vm.easyAnnouncement = {announcements:[],regions:[],event:{enabled:false,repeat:false,start:new Date(),monthly:[],color:'#039BE5'}};

    // vm.easyAnnouncement.event.weekly={"2":true};
    // Data
    vm.dialogData = dialogData;
    vm.easyAnnouncements= [];
    vm.myDate = new Date();
    vm.minDate = new Date(
      vm.myDate.getFullYear(),
      vm.myDate.getMonth(),
      vm.myDate.getDate());
    vm.minEndDate =new Date(
      vm.myDate.getFullYear(),
      vm.myDate.getMonth(),
      vm.myDate.getDate());
    vm.exists = msUtils.exists;
    vm.promptList = [
      {id:1,name:"welcome.wav"},
      {id:2,name:"goodbye1.wav"},
      {id:3,name:"goodbye.wav"},
      {id:4,name:"silent.wav"},
      {id:5,name:"working.wav"},
      {id:6,name:"welcome1.wav"},
      {id:7,name:"goodbye2.wav"},
      {id:8,name:"goodby3.wav"},
      {id:9,name:"silen4.wav"},
      {id:10,name:"workin5.wav"},
      {id:11,name:"welcom2.wav"},
      {id:12,name:"goodbye31.wav"},
      {id:13,name:"goodbye33.wav"},
      {id:14,name:"silent33.wav"},
      {id:15,name:"working33.wav"}
    ];
    vm.regionList = [{id:1,name:'Lahore',prefix:'042'},{id:2,name:'Karachi',prefix:'0423'},{id:3,name:'Islamabad',prefix:'0420'},
      {id:4,name:'Sawat',prefix:'052'},{id:5,name:'Faislabad',prefix:'0426'},
      {id:6,name:'Pinidi',prefix:'0422'}];
    vm.weekly=[{id:1,name:'Monday'},{id:2,name:'Tuesday'},{id:3,name:'Wednesday'},{id:4,name:'Thursday'},{id:5,name:'Friday'},{id:6,name:'Saturday'},{id:0,name:'Sunday'}];
    vm.days= $.map($(Array(31)), function (val, i) { return {'day':i + 1}; });
    vm.months= $.map($(Array(12)), function (val, i) { return i + 1; });

    // Methods
    vm.saveEvent = saveEvent;
    vm.closeDialog = closeDialog;
    vm.toggle = toggle;
    vm.promptName = promptName;
    vm.regionName = regionName;
    vm.filterAnnouncement = filterAnnouncement;
    vm.announcementQuerySearch = announcementQuerySearch;
    vm.regionQuerySearch = regionQuerySearch;
    vm.filterRegion = filterRegion;
    vm.toggleInArray = msUtils.toggleInArray;
    vm.setEndDate = setEndDate;
    vm.createInstanceEasyAnnouncement = createInstanceEasyAnnouncement;


    init();

    //////////

    function promptName(id){
      return _.result(_.find(vm.promptList, {id:id}), 'name');
    }
    function regionName(id){
      return _.result(_.find(vm.regionList, {id:id}), 'name');
    }
    function filterAnnouncement(label)
    {
      if ( !vm.labelSearchText || vm.labelSearchText === '' )
      {
        return true;
      }

      return angular.lowercase(label.name).indexOf(angular.lowercase(vm.labelSearchText)) >= 0;
    }
    function announcementQuerySearch(query)
    {
      return query ? vm.promptList.filter(createFilterFor(query)) : [];
    }
    function regionQuerySearch(query)
    {
      return query ? vm.regionList.filter(createFilterFor(query)) : [];
    }
    function filterRegion(label)
    {
      if ( !vm.regionSearchText || vm.regionSearchText === '' )
      {
        return true;
      }

      return angular.lowercase(label.name).indexOf(angular.lowercase(vm.regionSearchText)) >= 0;
    }
    /**
     * Initialize
     */
    function init()
    {
      vm.dialogTitle = (vm.dialogData.type === 'add' ? 'Add Easy Announcement' : 'Edit Easy Announcement');

      // Edit
      if ( vm.dialogData.calendarEvent )
      {
        // Clone the calendarEvent object before doing anything
        // to make sure we are not going to brake the Full Calendar
        vm.calendarEvent = angular.copy(vm.dialogData.calendarEvent);

       /* var p1 = new Promise(function(resolve,reject){
          resolve(createInstanceEasyAnnouncement(vm.calendarEvent));
        });*/
        // createEvents(vm.easyAnnouncement);
        createInstanceEasyAnnouncement(vm.calendarEvent).then(function(response){
          vm.easyAnnouncement = response;

        });

        // Convert moment.js dates to javascript date object
        if ( moment.isMoment(vm.easyAnnouncement.event.start) )
        {
          vm.easyAnnouncement.event.start = vm.calendarEvent.start.toDate();

        }

        if ( moment.isMoment(vm.easyAnnouncement.event.end) )
        {
          vm.easyAnnouncement.event.end = vm.calendarEvent.end.toDate();
        }
      }
      // Add
      else
      {

        // Convert moment.js dates to javascript date object
        if ( moment.isMoment(vm.dialogData.start) )
        {
          vm.dialogData.start = vm.dialogData.start.toDate();
          vm.easyAnnouncement.event.start =  vm.dialogData.start;
        }

        if ( moment.isMoment(vm.dialogData.end) )
        {
          vm.dialogData.end = vm.dialogData.end.toDate();
          vm.easyAnnouncement.event.end = vm.dialogData.end;
        }

        vm.calendarEvent = {
          start        : vm.dialogData.start,
          end          : vm.dialogData.end,
          notifications: []
        };
      }
    }
    function toggle(item, list) {
      var idx = list.indexOf(item);
      if (idx > -1) list.splice(idx, 1);
      else list.push(item);
    }

    function saveEvent()
    {

     /* var p1 = new Promise(function(resolve,reject){
        resolve(createEvents(vm.easyAnnouncement));
      });*/
      // createEvents(vm.easyAnnouncement);
      createEvents(vm.easyAnnouncement).then(function(val){
        var response = {
          type         : vm.dialogData.type,
          calendarEvent : val
        };
        $mdDialog.hide(response);
      });

    }
    function createInstanceEasyAnnouncement(calendarEvent){
      var defered = $q.defer();
      var easyAnnouncement = {};
      easyAnnouncement.id= calendarEvent.id;
      easyAnnouncement.name= calendarEvent.title;
      easyAnnouncement.description= calendarEvent.description;
      (calendarEvent.announcements===undefined)? easyAnnouncement.announcements=[] :easyAnnouncement.announcements= calendarEvent.announcements;
      (calendarEvent.regions===undefined)? easyAnnouncement.regions=[] :easyAnnouncement.regions= calendarEvent.regions;

      easyAnnouncement.placeholder= calendarEvent.placeholder;
      easyAnnouncement.event  = {
        start:calendarEvent.start.toDate(),
        end:undefined,
        startTime:$filter('date')(calendarEvent.start.toDate(),'HH:mm a'),
        repeat:calendarEvent.repeat,
        repeatType:calendarEvent.repeatType,
        color:calendarEvent.color,
        allDay:calendarEvent.allDay,
        weekly:calendarEvent.weekly,
        monthly:calendarEvent.monthly,
        daily:calendarEvent.daily

      };
      if(calendarEvent.end){
         easyAnnouncement.event.end = calendarEvent.end.toDate()
           easyAnnouncement.event.endTime=$filter('date')(calendarEvent.end.toDate(),'HH:mm a');

      }
      defered.resolve(easyAnnouncement);
      return defered.promise;
    }

    function setEndDate(){
      vm.easyAnnouncement.event.end=undefined;
      vm.minEndDate = new Date(
        vm.easyAnnouncement.event.start.getFullYear(),
        vm.easyAnnouncement.event.start.getMonth(),
        vm.easyAnnouncement.event.start.getDate());
    }
    function createEvents(easyAnnouncement){
      var defered = $q.defer();
      var events = [];
      var startDate = $filter('date')(easyAnnouncement.event.start,'yyyy-MM-dd');

      // var endDate = $filter('date',event.end,'yyyy-mm-dd');
      var endDate = easyAnnouncement.event.end;
      if(!endDate){
        endDate = new Date(vm.myDate.getFullYear(),vm.myDate.getMonth()+2,vm.myDate.getDate());
        // endDate= $filter('date')(endDate,'yyyy-MM-dd');
      }else{
        endDate=$filter('date')(easyAnnouncement.event.end,'yyyy-MM-dd');
      }
      var startMoment = moment(startDate);
      var endMoment = moment(endDate);
      var daysCount = endMoment.diff(startMoment,'days');
      if(easyAnnouncement.event.repeat){
        for(var current=0;current<=daysCount;current=current+1){
          var  ev  = {};
          if(easyAnnouncement.event.repeat){
            if(easyAnnouncement.event.repeatType==='weekly'){
              var weeklyDays = easyAnnouncement.event.weekly;
              var sDate = addDays(easyAnnouncement.event.start,current);
              if(weeklyDays[sDate.getDay()]){
                addEvents(easyAnnouncement,current).then(function(res){
                  events.push(res);
                });
              }
            }
           else if(easyAnnouncement.event.repeatType==='monthly'){
              var monthlyDays = easyAnnouncement.event.monthly;
              var sDate = addDays(easyAnnouncement.event.start,current);
              if(monthlyDays.indexOf(sDate.getDate().toString())!=-1){
                addEvents(easyAnnouncement,current).then(function(res){
                  events.push(res);
                });
              }
            }
            else{
              addEvents(easyAnnouncement,current).then(function(res){
                events.push(res);
              });
            }
          }





        }
      }else{
        addEvents(easyAnnouncement,'one').then(function(res){
          events.push(res);
        });

      }

      defered.resolve(events);
      return defered.promise;

    }

    function addEvents(easyAnnouncement,count){
      var defered = $q.defer();
      var start = easyAnnouncement.event.start;
      var end = start;
      var startDate = $filter('date')(vm.easyAnnouncement.event.start,'yyyy-MM-dd');
      if(vm.easyAnnouncement.event.startTime && !easyAnnouncement.event.allDay){
        var startTime = $filter('date')(vm.easyAnnouncement.event.startTime,'HH:mm A');

        start = moment(startDate + ' '+startTime).toDate();
      }
      else{

        start = moment(startDate + ' '+'00:00').toDate();
      }
      var endTime = '23:59';
      if(vm.easyAnnouncement.event.endTime && !easyAnnouncement.event.allDay){
        endTime = $filter('date')(vm.easyAnnouncement.event.endTime,'HH:mm A');

        end = moment(startDate + ' '+endTime).toDate();
      }else{

        end = moment(startDate + ' '+endTime).toDate();
      }

      if(count==='one'){
        var endDate = $filter('date')(vm.easyAnnouncement.event.end,'yyyy-MM-dd');
        end = moment(endDate + ' '+endTime).toDate();
        count=0;
      }

      var event = {};
      if(count===0){
        event =  {
          id:1,
          title: easyAnnouncement.name,
          start:start,
          end: end,
          allDay: easyAnnouncement.event.allDay,
          repeat:easyAnnouncement.event.repeat,
          repeatType:easyAnnouncement.event.repeatType,
          announcements:easyAnnouncement.announcements,
          regions:easyAnnouncement.regions,
          placeHolder:easyAnnouncement.place_holder,
          color:easyAnnouncement.event.color,
          weekly:easyAnnouncement.weekly,
          monthly:easyAnnouncement.monthly,
          daily:easyAnnouncement.daily
        }
      }
      else{
        event = {
          id:1,
          title: easyAnnouncement.name,
          start: addDays(start,count),
          end: addDays(end,count),
          allDay: easyAnnouncement.event.allDay,
          repeat:easyAnnouncement.event.repeat,
          repeatType:easyAnnouncement.event.repeatType,
          announcements:easyAnnouncement.announcements,
          regions:easyAnnouncement.regions,
          placeHolder:easyAnnouncement.place_holder,
          color:easyAnnouncement.event.color,
          weekly:easyAnnouncement.event.weekly,
          monthly:easyAnnouncement.event.monthly,
          daily:easyAnnouncement.event.daily


        }
      }
      defered.resolve(event);
      return defered.promise;
    }
    function addDays(date, days) {
      var result = new Date(date);
      result.setDate(date.getDate() + days);
      return result;
    }
    /**
     * Close the dialog
     */
    function closeDialog()
    {
      $mdDialog.cancel();
    }
    function createFilterFor(query)
    {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(item)
      {
        return angular.lowercase(item.name).indexOf(lowercaseQuery) >= 0;
      };
    }
  }

  function dateDirective() {
    return {
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModel) {
        var toView = function(val) {
          return val;
        };

        var toModel = function(val) {
          var offset = moment(val).utcOffset();
          var date = new Date(moment(val).add(offset, 'm'));
          return date;
        };

        ngModel.$formatters.unshift(toView);
        ngModel.$parsers.unshift(toModel);
      }
    };
  }

  function timePicker($timeout) {
    return {
      restrict: 'AC',
      scope: {
        ngModel: '='
      },
      link: function (scope, element) {
        element.on('change', function () {
          if (element.hasClass('start') && element.hasClass('time')) {
            $timeout(function () {
              var $el = element.closest('[date-pair]').find('input.time.end'),
                endScope = angular.element($el).isolateScope();

              endScope.$apply(function () {
                endScope.ngModel = $el.val();
              });
            }, 0);
          }
        });
        element.timepicker({
          'showDuration': false,
          'show2400':true,
          'timeFormat': 'H:i'
        });
      }
    };
  }
  function datePicker($timeout) {
    return {
      restrict: 'AC',
      scope: {
        ngModel: '='
      },
      link: function (scope, element) {
        element.on('change', function () {
          if (element.hasClass('start') && element.hasClass('date')) {
            $timeout(function () {
              var $el = element.closest('[date-pair]').find('input.date.end'),
                endScope = angular.element($el).isolateScope();

              endScope.$apply(function () {
                endScope.ngModel = $el.val();
              });
            }, 0);
          }
        });
        element.datepicker({
          'format': 'm/d/yyyy',
          'autoclose': true
        });
      }
    };
  }
  function datePair(){
    return {
      restrict: 'AC',
      link: function (scope, element) {
        element.datepair();
      }
    }
  }
})();
