/**
 * Created by mumar on 1/7/2016.
 */

(function ()
{
  'use strict';

  angular
    .module('app.agent', ['ngTable'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msNavigationServiceProvider)
  {
    // State
    $stateProvider.state('app.agent', {
      url      : '/agent',
      views    : {
        'content@app': {
          templateUrl: 'app/adminPanel/agent/list.html',
          controller : 'AgentController as vm'
        }
      },
      resolve  : {
        AgentData: function (apiResolver)
        {
          return apiResolver.resolve('agent.list@get');
        }
      }/*,
      bodyClass: 'todo'*/
    });
    $stateProvider.state('app.agent.details', {
      url      : '/:id',
      views    : {
        'detailsContent@app': {
          templateUrl: 'app/adminPanel/agent/list.html',
          controller : 'AgentController as vm'
        }
      },
      resolve  : {
        AgentData: function (apiResolver)
        {
          return apiResolver.resolve('agent.list@get');
        }
      }/*,
       bodyClass: 'todo'*/
    });


    // Navigation
    msNavigationServiceProvider.saveItem('agent', {
      title : 'Agents',
      icon  : 'icon-account-multiple',
      state:'app.agent'
    });
  }

})();
