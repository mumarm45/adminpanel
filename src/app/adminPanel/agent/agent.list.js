/**
 * Created by mumar on 1/7/2016.
 */
(function ()
{
  'use strict';

  angular
    .module('app.agent')
    .controller('AgentController',agentController)
    .controller('AgentDetailController',agentDetailController)
    .controller('AgentEditController',agentEditController);

  function agentController($scope,AgentData,$log,$document,$filter,$mdSidenav,$mdDialog){
    var vm = this;
    vm.agentList =  AgentData.agentList;
    vm.groupList=AgentData.groupList;
    vm.skillList = AgentData.skillList;


    vm.selectedUserIndex = undefined;
    vm.selectUserIndex = function (index) {
      if (vm.selectedUserIndex !== index) {
        vm.selectedUserIndex = index;
      }
      else {
        vm.selectedUserIndex = undefined;
      }
    };
    vm.getCheckedUsers =function(){
      return $filter('filter')(vm.agentList,{checked:true});
    };
    vm.skillName = function(id){
      return _.result(_.find(vm.skillList, {id:id}), 'name');
    };
    vm.groupName = function(id){
      return _.result(_.find(vm.groupList, {id:id}), 'name');
    };
    vm.allChecked=function(){

      return vm.getCheckedUsers().length == vm.agentList.length ;
    };
    vm.checkAll = function(value){

      angular.forEach(vm.agentList,function(bu){
        bu.checked=value;
      })
    };
    vm.getCheckedIds = function(){
      return _.pluck(vm.getCheckedUsers(),'id');
    };
    vm.dialogShowDetail = function(agent,e){
      $mdDialog.show({
        controller         : 'AgentEditController',
        controllerAs       : 'vm',
        templateUrl        : 'app/adminPanel/agent/edit.html',
        parent             : angular.element($document.body),
        targetEvent        : e,
        clickOutsideToClose: true,
        locals             : {
          agent      : agent,
          skillList      : vm.skillList,
          groupList      : vm.groupList
        }
      }).then(function(agentData){
        var ind=_.findIndex(vm.agentList,{userId:agentData.userId});
        vm.agentList[ind]=agentData;
      })
    };

    function agentEditForm(agent,skillList,groupList,e){
      $mdDialog.show({
        controller         : 'AgentEditController',
        controllerAs       : 'vm',
        templateUrl        : 'app/adminPanel/agent/edit.html',
        parent             : angular.element($document.body),
        targetEvent        : e,
        clickOutsideToClose: true,
        locals             : {
          agent      : agent,
          skillList      : skillList,
          groupList      : groupList
        }
      }).then(function(agentData){
        var ind=_.findIndex(vm.agentList,{userId:agentData.userId});
        vm.agentList[ind]=agentData;
      })
    }

  }
  function agentDetailController(agent,event,agentEditForm,$mdDialog,skillList,groupList){
    var vm = this;
    vm.agent = agent;
    vm.editAgentDialog = editAgentDialog;

    vm.groupList = [
      {"id":1,"name":"sell"},
      {"id":2,"name":"sell1"},
      {"id":3,"name":"sell2"},{"id":4,"name":"sell3"},
      {"id":5,"name":"sell4"}
    ];
    vm.skillName = function(id){
      return _.result(_.find(skillList, {id:id}), 'name');
    };
    vm.groupName = function(id){
      return _.result(_.find(groupList, {id:id}), 'name');
    };
    function closeDialog()
    {
      $mdDialog.hide();
    }

    function editAgentDialog(updateAgent){
      agentEditForm(updateAgent,skillList,groupList);
    }

  };
  function agentEditController(agent,skillList,groupList,$mdDialog){
    var vm = this;
    vm.agent = agent;
    vm.saveAgent = saveAgent;
    vm.groupList = groupList;
    vm.addNewSkill = addNewSkill;
    vm.closeDialog = closeDialog;

    vm.skillName = function(id){
      return _.result(_.find(skillList, {id:id}), 'name');
    };
    vm.groupName = function(id){
      return _.result(_.find(groupList, {id:id}), 'name');
    };
    function closeDialog()
    {
      $mdDialog.hide();
    }
   function saveAgent(response){
     $mdDialog.hide(response);
   }
    function addNewSkill(){
      var newSkill = vm.agent.skillId.pop();
      var foundSkill = _.find(skillList, {name:newSkill});
      if(foundSkill===undefined){
        var count = skillList.length+1;
        skillList.push({id:count,name:newSkill});
        vm.agent.skillId.push({id:count});

      }
    }


  };






})();
