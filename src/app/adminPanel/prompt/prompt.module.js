(function ()
{
    'use strict';

    angular
        .module('app.prompt', ['angularFileUpload','ngAudio'])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
          .state('app.prompts', {
            url      : '/prompt/:id',
            views    : {
                'content@app': {
                    templateUrl: 'app/adminPanel/prompt/prompt.html',
                    controller : 'PromptController as vm'
                }
            },
            resolve  : {
              Prompts: function (apiResolver,$stateParams)
              {
                //return PromptService.getPrompt({id:$stateParams.id});
                if($stateParams.id==="")
                {
                  return  apiResolver.resolve('prompts.list@get');

                }else{
                  return  apiResolver.resolve('prompts.'+$stateParams.id+'@get');

                }



              }
            },
            bodyClass: 'file-manager'
        })
    .state('app.prompts.prompt', {
      url    : '/:id',
      views  : {

        'content@app': {
          templateUrl: 'app/adminPanel/prompt/prompt.html',
          controller : 'PromptController as vm'
        }
      },
      resolve: {
        Prompts: function (apiResolver,$stateParams)
        {
          //return PromptService.getPrompt({id:$stateParams.id});
          if($stateParams.id!=''){
            return  apiResolver.resolve('prompts.list@get');

          }else{
            return  apiResolver.resolve('prompts.'+$stateParams.id+'@get');

          }



        }
      }
    });

        // Translation
    //    $translatePartialLoaderProvider.addPart('app/main/apps/file-manager');

        // Navigation
        msNavigationServiceProvider.saveItem('prompts', {
            title : 'Prompts',
            icon  : 'icon-folder',
            state : 'app.prompts({id:""})',
            weight: 1
        });
    }

})();
