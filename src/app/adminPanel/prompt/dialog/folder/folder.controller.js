/**
 * Created by mumar on 2/3/2016.
 */
(function(){
  'use strict';
  angular
    .module('app.prompt')
    .controller('FolderDialogController',FolderDialogController);

  function FolderDialogController($mdDialog){
    var vm = this;
    vm.createFolder = createFolder;
    vm.closeDialog = closeDialog;


    function createFolder(folder){
      $mdDialog.hide(folder);

    }
    function closeDialog(){
      $mdDialog.hide();
    }


  }
})();
