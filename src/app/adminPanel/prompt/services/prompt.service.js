/**
 * Created by mumar on 2/2/2016.
 */
(function(){
  'use strict';
  angular
    .module('app.prompt')
    .factory('PromptService',PromptService);

  function PromptService($resource,$q,$http){
  var appBaseU = 'http://192.168.1.32/adminapi';
    //$http.defaults.headers.common["Accept"]='Application/JSON';
    //$http.defaults.headers.common["Authorization"]='Basic YWRtaW5pc3RyYXRvcjpFeHBlcnRmbG93NDY0';
    //$http.defaults.headers.common["Content-type"]='Application/JSON';
    var prompts = $resource(appBaseU+'/prompt',{id:'@id'},{
      getPrompts:{
         method:'GET',
        headers:{
          'Accept':'Application/JSON','Authorization':'Basic YWRtaW5pc3RyYXRvcjpFeHBlcnRmbG93NDY0','Content-type':'Application/JSON'

        },
         url:appBaseU+"/prompt/:id"
      }

    });
    return{
      'getPrompt':function(id){
        var defered = $q.defer();
        prompts.getPrompts(id,function(data){
          defered.resolve(data);
        },function(er){
          defered.reject(er);
        });
        return defered.promise;
      }
    }
  }


})();
