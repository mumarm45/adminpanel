(function ()
{
  'use strict';

  angular
    .module('app.prompt')
    .controller('PromptController', PromptController);

  /** @ngInject */
  function PromptController(msUtils,$mdSidenav,$filter,$state,Prompts,PromptService,$log,$stateParams,$mdDialog,$document,ngAudio)
  {
    var vm = this;

    // Data
    vm.accounts = {
      'creapond'    : 'johndoe@creapond.com',
      'withinpixels': 'johndoe@withinpixels.com'
    };
    vm.selectedAccount = 'creapond';
    vm.currentView = 'list';
    vm.showDetails = true;
    // vm.init = init;
    vm.languageList =[
       {
         'id':1,
        'name'      : 'English',
        'translation': 'TOOLBAR.ENGLISH',
        'code'       : 'en',
        'flag'       : 'us'
      },
       {
         'id':2,
        'name'      : 'German',
        'translation': 'TOOLBAR.German',
        'code'       : 'ug',
        'flag'       : 'ug'
      },
       {
         'id':3,
        'name'      : 'French',
        'translation': 'TOOLBAR.French',
        'code'       : 'fr',
        'flag'       : 'fr'
      }
  ];


    // Methods
    vm.select = select;
    vm.toggleDetails = toggleDetails;
    vm.toggleSidenav = toggleSidenav;
    vm.toggleView = toggleView;
    vm.deleteRe = deleteRe;
    vm.hidden = false;
    vm.isOpen = false;
    vm.hover = true;
    vm.showFolderDialog= showFolderDialog;
    vm.showFileDialog= showFileDialog;
    vm.openFolder= openFolder;
    vm.dateConvert= dateConvert;
    vm.toggleInArray = msUtils.toggleInArray;
    vm.exists = msUtils.exists;
    vm.filterLanguage = filterLanguage;
    vm.languageName = languageName;
    //////////
    function filterLanguage(language)
    {
      if ( !vm.languageSearchText || vm.languageSearchText === '' )
      {
        return true;
      }

      return angular.lowercase(language.name).indexOf(angular.lowercase(vm.languageSearchText)) >= 0;
    }
    function languageName(id){
      return _.find(vm.languageList, {id:id});
    }
    function languageQuerySearch(query)
    {
      return query ? vm.languageList.filter(createFilterFor(query)) : [];
    }
    function createFilterFor(query)
    {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(item)
      {
        return angular.lowercase(item.name).indexOf(lowercaseQuery) >= 0;
      };
    }
    vm.folders = Prompts.Prompt.Folder;
    vm.path = vm.folders[0].path.split('/');
    vm.path[0] = "Prompt";
    vm.files = Prompts.Prompt.File;
    if(vm.files!=undefined){
      vm.selected = vm.files[0];
      vm.selected.type='file';
      vm.sound = ngAudio.load("../app/adminPanel/prompt/welcome.mp3");
      vm.selected.languages=[];
    }
    else{
      vm.selected= vm.folders[0];
      var datee = vm.selected.Details.dateModified;
      vm.selected.type='folder';
      vm.selected.languages=[];
    }

    /**
     * Select an item
     *
     * @param item
     */
    function select(item,type)
    {
      if(type==='file'){
        vm.sound.pause();
        item.type='file';
        vm.sound = ngAudio.load("../app/adminPanel/prompt/welcome.mp3");

      }else{
        item.type="Folder";
      }
      vm.selected = item;
      vm.selected.languages=[];
    }
    function dateConvert(date){
      var da = $filter('limitTo')(date, 22, 0);
      return moment(da).fromNow();
    };
    function openFolder(folderName){
      $state.go('app.prompts',{id:folderName});
    }
    function showFolderDialog(e)
    {
      $mdDialog.show({
        controller         : 'FolderDialogController',
        controllerAs       : 'vm',
        templateUrl        : 'app/adminPanel/prompt/dialog/folder/create.html',
        parent             : angular.element($document.body),
        targetEvent        : e,
        clickOutsideToClose: true,
        locals             : {
          event              : e
        }
      }).then(function(response){
        vm.folders.push(
          {"path":vm.folders[0].path,
            "FolderName":response.name,
          "Details":{"size":"47.2 KB","dateModified":new Date(),"modifiedBy":"Administrator"
          }
          })
      })
    }
    function showFileDialog( e)
    {
      $mdDialog.show({
        controller         : 'FileDialogController',
        controllerAs       : 'vm',
        templateUrl        : 'app/adminPanel/prompt/dialog/file/create.html',
        parent             : angular.element($document.body),
        targetEvent        : e,
        clickOutsideToClose: true,
        locals             : {
          event              : e
        }
      }).then(function(response){
        vm.folders.push(
          {"path":vm.folders[0].path,
            "FolderName":response.name,
            "Details":{"size":"47.2 KB","dateModified":new Date(),"modifiedBy":"Administrator"
            }
          })
      })
    }
    /**
     * Toggle details
     *
     * @param item
     */
    function toggleDetails(item)
    {
      vm.selected = item;
      toggleSidenav('details-sidenav');
    }

    function deleteRe(id,type){
      if(type==='file'){
        _.remove(vm.files,function(fl){
          return id.FileName===fl.FileName;
        })
      }else{
        _.remove(vm.folders,function(fl){
          return id.FolderName===fl.FolderName;
        })
      }
    }

    /**
     * Toggle sidenav
     *
     * @param sidenavId
     */
    function toggleSidenav(sidenavId)
    {
      $mdSidenav(sidenavId).toggle();
    }

    /**
     * Toggle view
     */
    function toggleView()
    {
      vm.currentView = vm.currentView === 'list' ? 'grid' : 'list';
    }
  }
})();
