/**
 * Created by mumar on 2/17/2016.
 */
(function(){
  'use strict';
  angular
    .module('app.application',[])
    .config(config);

  function config($stateProvider,msNavigationServiceProvider){
    // State
    $stateProvider.state('app.application', {
      url      : '/application',
      views    : {
        'content@app': {
          templateUrl: 'app/adminPanel/application/application.html',
          controller : 'ApplicationController as vm'
        }
      },
      resolve: {
        ApplicationData: function (apiResolver)
        {
            return  apiResolver.resolve('applications.list@get');
        },
        TriggerData: function (apiResolver)
        {
          return  apiResolver.resolve('applications.triggers@get');
        },
        ScriptData: function (apiResolver)
        {
          return  apiResolver.resolve('applications.scripts@get');
        } ,Adnan_Hello: function (apiResolver)
        {
          return  apiResolver.resolve('applications.Adnan_Hello@get');
        }
      },
      bodyClass: 'calendar'
    });


    // Navigation
    msNavigationServiceProvider.saveItem('application', {
      title : 'Applications',
      icon  : 'icon-package',
      state : 'app.application',
      weight: 1
    });

  }
})();
