/**
 * Created by mumar on 2/2/2016.
 */
(function(){
  'use strict';
  angular
    .module('app.application')
    .factory('ApplicationService',ApplicationService);

  function ApplicationService($resource,$q,$http){
  var appBaseU = 'http://192.168.1.32/adminapi';

    var application = $resource(appBaseU+'/application',{id:'@id'},{
      getApplication:{
         method:'GET',
        headers:{
          'Accept':'Application/JSON','Authorization':'Basic QWRtaW5pc3RyYXRvcjpFeHBlcnRmbG93NDY0','Content-type':'Application/JSON'

        },
         url:appBaseU+"/application/:id"
      }

    });
    return{
      'getApplication':function(id){
        var defered = $q.defer();
        application.getApplication(id,function(data){
          defered.resolve(data);
        },function(er){
          defered.reject(er);
        });
        return defered.promise;
      }
    }
  }


})();
