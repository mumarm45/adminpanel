/**
 * Created by mumar on 2/17/2016.
 */
(function(){
  'use strict';
  angular
    .module('app.application')
    .controller('ApplicationController',applicationController);

  function applicationController(ApplicationData,ScriptData,Adnan_Hello,$mdDialog,$document,ApplicationService,msUtils){
    var vm = this;

    vm.openCreateApplicationDialog = openCreateApplicationDialog;

    vm.applications = ApplicationData.application;
    vm.scripts = ScriptData.Script.File;
    vm.dataScript = Adnan_Hello.Adnan_Hello;
    vm.languageList =[
      {
        'id':1,
        'name'      : 'English',
        'translation': 'TOOLBAR.ENGLISH',
        'code'       : 'en',
        'flag'       : 'us'
      },
      {
        'id':2,
        'name'      : 'German',
        'translation': 'TOOLBAR.German',
        'code'       : 'ug',
        'flag'       : 'ug'
      },
      {
        'id':3,
        'name'      : 'French',
        'translation': 'TOOLBAR.French',
        'code'       : 'fr',
        'flag'       : 'fr'
      }
    ];

    //methods
    vm.toggleSidenav = toggleSidenav;
    vm.toggleDetails = toggleDetails;
    vm.select = select;
    vm.filterLanguage = filterLanguage;
    vm.languageName = languageName;
    vm.toggleInArray = msUtils.toggleInArray;
    vm.exists = msUtils.exists;
    vm.languageQuerySearch = languageQuerySearch;
    vm.selected ={languages:[]};
    vm.types = ['Cisco Script Application','Busy','Ring-No-Answer'];
    function select(item)
    {

      vm.selected = item;
      vm.scriptName=vm.selected.ScriptApplication.script.split('[')[1].replace(']','');
      vm.selected.script = vm.scriptName;
      ApplicationService.getApplication({id:vm.selected.applicationName}).then(function(response){
     //   vm.selected.script = vm.scriptName;
        vm.scriptparams = response.ScriptApplication.scriptParams;
      },function(error){
        console.log(error);
      });

      vm.selected.languages=[];
    }
    function openCreateApplicationDialog(ev) {
      $mdDialog.show({
        templateUrl        : 'app/adminPanel/application/dialog/create.html',
        controller         : "CreateApplicationController",
        controllerAs       : "vm",
        parent             : $document.find('#scrumboard'),
        targetEvent        : ev,
        clickOutsideToClose: true,
        escapeToClose      : true,
        locals             : {
          event              : ev,
          scripts:vm.scripts
        }
      }).then(function(response){
        vm.applications.push(response);
      })

    }
    function toggleDetails(item)
    {
      vm.selected = item;
      toggleSidenav('details-sidenav');
    }
    function toggleSidenav(sidenavId)
    {
      $mdSidenav(sidenavId).toggle();
    }
    function filterLanguage(language)
    {
      if ( !vm.languageSearchText || vm.languageSearchText === '' )
      {
        return true;
      }

      return angular.lowercase(language.name).indexOf(angular.lowercase(vm.languageSearchText)) >= 0;
    }
    function languageName(id){
      return _.find(vm.languageList, {id:id});
    }
    function languageQuerySearch(query)
    {
      return query ? vm.languageList.filter(createFilterFor(query)) : [];
    }
    function createFilterFor(query)
    {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(item)
      {
        return angular.lowercase(item.name).indexOf(lowercaseQuery) >= 0;
      };
    }
  }


})();
